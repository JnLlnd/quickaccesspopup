@ECHO OFF
rem Set environment variables for Inno Setup and the batch Compile-QAP-v8.bat
SET QAPVERSIONPREV=8_7_1
SET QAPVERSION=8_7_1_1
SET QAPVERSIONNUMBER=8.7.1.1
SET QAPVERSIONTEXT=v%QAPVERSIONNUMBER%
SET QAPBETAPROD=
rem Call Compile batch
CALL "E:\Dropbox\AutoHotkey\QuickAccessPopup\Setup Script files\Compile-QAP-v8.bat"
